# This script plots the wind properties

from vader import readCheckpoint
import numpy as np
import os.path as osp
import glob
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.colorbar as cbar
import matplotlib.colors as colors
import numpy.ma as ma
from scipy.special import erf

# Units
import scipy.constants as physcons
G = physcons.G*1e3
import astropy.units as astropyu
Msun = astropyu.Msun.to('g')
pc = astropyu.pc.to('cm')
yr = astropyu.yr.to('s')
Myr = 1e6*yr
Gyr = 1e9*yr
kmps = 1e5

# Directory where data lives
dirname = '/Users/krumholz/Data/cmz/'

# List of run parameters
mdot = ['0.1', '0.3', '1.0']
er = '0.10'
epsff = '0.010'
enwind = '0'

# Set up plot
plt.figure(1, figsize=(6,4))
plt.clf()
colors=['r', 'g', 'b']
plts=[]
labels=[]

# Loop over runs
for i in range(len(mdot)):

    # Read checkpoint
    basename = osp.join(dirname, 'mdot'+mdot[i]+'_er'+er+'_epsff' + \
                        epsff+'_enwind'+enwind+'_?????.vader')
    files = glob.glob(basename)
    files.sort()
    chkname = files[-1]
    data = readCheckpoint(chkname)

    # Extract data
    r = data.grid.r
    vphi = data.grid.vphi_g[1:-1]
    beta = data.grid.beta_g[1:-1]
    area = data.grid.area
    t = data.tOut
    col = data.colOut
    pres = data.presOut
    sigma = np.sqrt(pres/col)
    colwind = data.userOut[:,5,:]
    mdotwind = np.sum(colwind*area, axis=1)
    pdot = data.userOut[:,2,:]
    edot = data.userOut[:,3,:]
    mdotin = -data.mBndOut[:,1]/(t+1.0e-50)
    mdotin[0] = mdotin[1]
    colsfr = data.userOut[:,4,:]
    colsfr_obs = data.userOut[:,6,:]
    mdotin = -data.mBndOut[:,1]/(t+1.0e-50)
    mdotin[0] = mdotin[1]

    # Compute scale height, virial ratio, depletion time
    rhostar = 2.5*vphi**2*(1+2*beta)/(4*np.pi*G*r**2)
    zetad = 0.33
    a = 2.0*np.pi*zetad*G*rhostar*col
    b = np.pi/2.0*G*col**2
    c = -pres
    h = (-b + np.sqrt(b**2-4*a*c))/(2*a)
    alphavir = pres/(np.pi/2.0*G*col**2*h)
    tdep = col/(colsfr+1.0e-50)

    # Compute wind parameters
    pdotedd = 2.0*np.pi*G*col*(col+rhostar*r/2.5)
    xcrit = np.log((pdot+1.0e-50)/pdotedd)
    ggas = 2.0*np.pi*G*col
    gstar = 2.0*np.pi*G*rhostar*r/2.5
    vesc = np.sqrt(4.0*(ggas+gstar)*r)
    #vesc = 2.0*np.pi*G*(col+rhostar*h)*(h/sigma)
    #vesc = np.sqrt(4.0*np.pi*G*(col+rhostar*h)*h)
    mach = sigma/(0.5*kmps)
    rturb = 0.5*(3.0-2.5)/(2.0-2.5)*(1-mach**(2*(2.0-2.5))) \
            / (1-mach**(2*(3.0-2.5)))
    sigma_surf2 = np.log(1.0+rturb*mach**2/4.0)
    zeta = 0.5*(1.0-erf((-2.0*xcrit+sigma_surf2) / np.sqrt(8.0*sigma_surf2)))


    # Sub-sample
    interval=10
    colwindsub = colwind[::interval, :]
    vescsub = vesc[::interval, :]
    xcritsub = xcrit[::interval, :]
    sigma_surf2sub = sigma_surf2[::interval, :]

    # Set up a grid of velocities for escaping gas
    vgrid = np.linspace(300,700,101)*kmps

    # Make grid of v/vesc
    u = np.einsum('i,jk->ijk', vgrid, 1/vescsub)
    u = ma.masked_array(u, mask=u<=1)
    #xv = xcritsub - np.log(u-1)
    xv = xcritsub - np.log(u**2-1)
    pxv = 1.0/np.sqrt(2.0*np.pi*sigma_surf2sub) * \
          np.exp(-(xv-sigma_surf2sub/2)**2/(2.0*sigma_surf2sub))
    #dmdv = 1.0/(u-1)*pxv
    dmdv = 2.0*u/(u**2+1)*pxv

    # Normalize the distributions so \int (dm/dv) dv = 1
    dmdv = dmdv / ((vgrid[1]-vgrid[0])*np.sum(dmdv, axis=0))
    
    # Compute wind mass flux-weighted sum
    dmdvsum = np.sum(dmdv*colwindsub*area, axis=2)

    # Compute time average at t > 200 Myr
    dmdvavg = np.mean(dmdvsum[:,t[::interval]>200*Myr], axis=1)

    # Get range and sort
    dmdvdiff = np.transpose(np.transpose(
        dmdvsum[:,t[::interval]>200*Myr],)-dmdvavg)
    dmdvdiffsort = np.sort(dmdvdiff, axis=-1)

    # Plot
    nidx = dmdvdiffsort.shape[1]
    p1,=plt.plot(vgrid/kmps, dmdvavg/(Msun/kmps/yr), colors[i], lw=2)
    plts.append(p1)
    labels.append(('{:3.1f}'.format(float(mdot[i]))
                   +' $M_\odot$ yr$^{-1}$'))
    plt.fill_between(vgrid/kmps,
                     (dmdvavg+dmdvdiffsort[:,nidx*0.1])/(Msun/kmps/yr),
                     (dmdvavg+dmdvdiffsort[:,nidx*0.9])/(Msun/kmps/yr),
                     color=colors[i], alpha=0.2)
    plt.xlim([300, 700])
    plt.ylim([3e-5, 1e-1])
    plt.xlabel(r'$v$ [km s$^{-1}$]')
    plt.ylabel(r'$d\dot{M}_{\mathrm{wind}}/dv$ [$M_\odot$ yr$^{-1}$ (km s$^{-1}$)$^{-1}$]')
    plt.yscale('log')

# Legend and spacing
plt.legend(plts, labels, loc='upper right')
plt.subplots_adjust(bottom=0.15, left=0.15)

# Save
plt.savefig('wind.pdf')
