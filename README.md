# README #

This repository contains the code required to reproduce the results in Krumholz, Kruijssen, & Crocker (2016, submitted to MNRAS).

### Requirements ###

To run this code you will need the following:

* The [VADER](https://bitbucket.org/krumholz/vader) code, version [36df059](https://bitbucket.org/krumholz/vader/commits/36df0598dc3db695a2f8b40239a4ce88f8f70adb) or later
* Relatively recent versions of numpy and scipy

You will have to edit the various .py files to point to appropriate directories on your system.

### What's Included? ###

The scripts in this repository both use [VADER](https://bitbucket.org/krumholz/vader) to run a set of simulations, and then analyze the results.

To run the simulations:

* allruns.py: this performs all the simulations, and can be executed just by doing ``python allruns.py``, after editing to set the directory pointers properly for your machine
* runsim.py: a helper script called by allruns.py; no need to touch this
* template.param: a template parameter file for the [VADER](https://bitbucket.org/krumholz/vader) runs; no need to touch this
* launhardt02.dat: data file containing the Galactic center rotation curve of [Laundhardt, Zylka, & Mezger (2002)](http://adsabs.harvard.edu/abs/2002A%26A...384..112L); no need to touch this

To post-process the results, you will have to edit files to point to the locations where your simulation outputs are stored. After doing so:

* compare_\*.py, plot_\*.py, ksplot.py: these scripts produce all the plots in the paper; the mapping from script name to paper figure should be clear from the name. Each can be run just by doing ``python filename.py``.
* molinari_map.jpg, orbit_KDL15.dat: data / image files taken from the literature that are used in producing some of the plots.

### License ###

This code is released under the terms of the [GPL v3.0](http://www.gnu.org/licenses/gpl-3.0.en.html). The license is included in the repository.

### Contact information ###

For questions, please contact [Mark Krumholz](http://www.mso.anu.edu.au/~krumholz/).