# This script compares the runs with different values of epsilon_ff

from vader import readCheckpoint
import numpy as np
import os.path as osp
import glob
from scipy.signal import periodogram

# Units
import scipy.constants as physcons
G = physcons.G*1e3
import astropy.units as astropyu
Msun = astropyu.Msun.to('g')
pc = astropyu.pc.to('cm')
yr = astropyu.yr.to('s')
Myr = 1e6*yr
Gyr = 1e9*yr
kmps = 1e5

# Directory where data lives
dirname = '/Users/krumholz/Data/cmz/test1/'

# List of run parameters
mdot = [    '0.3',   '0.3',   '0.3']
er = [    '0.050', '0.050', '0.050']
epsff = [ '0.005', '0.010', '0.020']
nruns = len(mdot)

# Read the data for all runs
data = []
for i in range(nruns):

    # Read latest checkpoint
    basename = osp.join(dirname, 'mdot'+mdot[i]+'_er'+er[i]+'_epsff' + \
                        epsff[i]+'_?????.vader')
    files = glob.glob(basename)
    files.sort()
    chkname = files[-1]
    data.append(readCheckpoint(chkname))

# Make overall plot
plt.figure(1, figsize=(8,7.5))
plt.clf()

# Set up subplots
plt.subplot(3,nruns,1)
plt.subplots_adjust(hspace=0, wspace=0, bottom=0.5, top=0.94, right=0.95)

# Set up labels on runs
label = [ 'm{:02d}r{:03d}f{:02d}'.format
          (int(10*float(m)), int(1000*float(r)), int(1000*float(f)))
          for (m,r,f) in zip(mdot, er, epsff) ]

# Loop over runs
for i, d in enumerate(data):

    # Grab data from this run
    t = d.tOut
    r = d.grid.r
    area = d.grid.area
    col = d.colOut
    colsfr = d.userOut[:,4,:]
    colwind = d.userOut[:,5,:]
    colsfr_obs = d.userOut[:,6,:]
    mdotin = -d.mBndOut[:,1]/(t+1.0e-50)
    mdotin[:] = mdotin[1]

    # Get derived quantities
    mdotwind = np.sum(colwind*area, axis=1)
    #idxmax = np.argmax(colsfr*(r>50*pc), axis=1)
    idxmax = np.argmax(np.mean(colsfr, axis=0))
    idx10 = np.abs(np.subtract.outer(r[idxmax], r)) < 10*pc
    mgas = np.sum(col*area, axis=1)
    mgas10 = np.sum(col*idx10*area, axis=1)
    sfr = np.sum(colsfr*area, axis=1)
    sfr10 = np.sum(colsfr*idx10*area, axis=1)
    sfr_obs = np.sum(colsfr_obs*area, axis=1)
    sfr10 = np.sum(colsfr*idx10*area, axis=1)
    tdep = mgas / sfr
    tdep10 = mgas10 / sfr10
    tdep_obs = mgas / sfr_obs

    # Plot SFR and wind mass loss rate
    plt.subplot(3,nruns,1+i)
    plt.title(label[i])
    plt.plot(t/Myr, sfr/(Msun/yr), 'b', lw=2, label='SF')
    plt.plot(t/Myr, mdotwind/(Msun/yr), 'r', lw=2, label='Wind')
    plt.plot(t/Myr, mdotin/(Msun/yr), 'k--', lw=2, label='Inflow')
    plt.xlim([0,500])
    plt.gca().get_xaxis().set_ticklabels([])
    plt.yscale('log')
    plt.ylim([1e-3, 50])
    if i==0:
        plt.ylabel(r'$\dot{M}$ [$M_\odot$ yr$^{-1}$]')
    else:
        plt.gca().get_yaxis().set_ticklabels([])
    if i==nruns/2:
        ax1save = plt.gca()

    # Add legend
    if i == nruns-1:
        leg = plt.legend(ncol=3, bbox_to_anchor=(-0.5, 0.99),
                         prop = { 'size': 10 }, loc='upper center')

    # Plot depletion time
    plt.subplot(3,nruns,1+nruns+i)
    plt.plot(t/Myr, tdep/Gyr, 'k', lw=2, label='Whole CMZ')
    plt.plot(t/Myr, tdep10/Gyr, 'g', lw=2, label=r'Ring')
    plt.yscale('log')
    plt.xlim([0,500])
    plt.ylim([0.01, 50])
    plt.gca().get_xaxis().set_ticklabels([])
    if i==0:
        plt.ylabel(r'$t_{\mathrm{dep}}$ [Gyr]')
    else:
        plt.gca().get_yaxis().set_ticklabels([])

    # Add legend
    if i == nruns-1:
        leg = plt.legend(ncol=2, bbox_to_anchor=(-0.5, 0.99),
                         prop = { 'size': 10 }, loc='upper center')

    # Gas mass
    plt.subplot(3,nruns,1+2*nruns+i)
    plt.plot(t/Myr, mgas/Msun, 'k', lw=2, label='Whole CMZ')
    plt.plot(t/Myr, mgas10/Msun, 'g', lw=2, label='Ring')
    plt.xlim([0,500])
    plt.ylim([1e6,1e8])
    plt.yscale('log')
    plt.yticks([1e6,1e7])
    if i==0:
        plt.ylabel('$M_{\mathrm{gas}}$ [$M_\odot$]', labelpad=15)
    else:
        plt.gca().get_yaxis().set_ticklabels([])
    if i != nruns-1:
        plt.xticks([0,100,200,300,400])
    else:
        plt.xticks([0,100,200,300,400,500])
    if i == nruns/2:
        plt.xlabel(r'$t$ [Myr]')

    # Plot periodogram
    idx = t>200*Myr
    f, pxx = periodogram(sfr[idx], fs=1.0/(t[1]-t[0]), window='hann')
    f, pxx_obs = periodogram(sfr_obs[idx], fs=1.0/(t[1]-t[0]), window='hann')
    axcur = plt.gca().get_position()
    axcur.y0 = 0.3
    axcur.y1 = 0.425
    ax = plt.gcf().add_axes(axcur)
    ax.plot(1/(f*Myr), pxx/np.amax(pxx), 'k', lw=2, label='SFR')
    ax.plot(1/(f*Myr), pxx_obs/np.amax(pxx_obs), 'k--', lw=2,
            label='SFR (observed)')
    plt.xlim([1,100])
    plt.ylim([0,1.5])
    plt.xscale('log')
    plt.yticks([0,0.25,0.5,0.75,1, 1.25])
    if i == 0:
        plt.ylabel('Relative power', labelpad=8)
    else:
        ax.set_yticklabels([])
    if i == nruns/2:
        plt.xlabel(r'$\nu^{-1}$ [Myr]')
    if i != nruns-1:
        ax.set_xticks([1,10])
    else:
        ax.set_xticks([1,10,100])

    # Add legend
    if i == nruns-1:
        leg = plt.legend(ncol=2, bbox_to_anchor=(-0.5, 0.99),
                         prop = { 'size': 10 }, loc='upper center')

    # Plot PDF
    binwidth = 0.1
    binedges = np.arange(-2,1.5001,binwidth)
    hist, edges = np.histogram(np.log10(tdep[idx]/Gyr),
                               binedges,
                               density=True)    
    hist_obs, edges = np.histogram(np.log10(tdep_obs[idx]/Gyr),
                                   binedges,
                                   density=True)    
    tdepctr = 0.5*(edges[1:]+edges[:-1])
    axcur = plt.gca().get_position()
    axcur.y0 = 0.075
    axcur.y1 = 0.2
    ax = plt.gcf().add_axes(axcur)
    plt.plot(tdepctr, hist, 'k', lw=2, label='All')
    plt.plot(tdepctr, hist_obs, 'k--', lw=2, label='SF (observed)')
    plt.xlim([-2,1.5])
    plt.ylim([10.**-1.5,10])
    plt.yscale('log')
    if i == 0:
        plt.ylabel(r'$dp/d\,\log\,t_{\mathrm{dep}}$', labelpad=8)
    else:
        ax.set_yticklabels([])
    if i == nruns/2:
        plt.xlabel(r'$\log\, t_{\mathrm{dep}}$ [Gyr]')
    if i == nruns-1:
        plt.xticks([-2, -1, 0, 1])
    else:
        plt.xticks([-2, -1, 0, 1])

plt.savefig('compare_epsff.pdf')

