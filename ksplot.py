# This script makes a KS plot with all the simulations on it

from vader import readCheckpoint
import numpy as np
import os.path as osp
import glob
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.colorbar as cbar
import matplotlib.colors as colors

# Units
import scipy.constants as physcons
G = physcons.G*1e3
import astropy.units as astropyu
Msun = astropyu.Msun.to('g')
pc = astropyu.pc.to('cm')
yr = astropyu.yr.to('s')
Myr = 1e6*yr
Gyr = 1e9*yr
kmps = 1e5

# Directory where data lives
dirname = '/Users/krumholz/Data/cmz/test1/'

# List of run parameters
mdot = [    '0.1',   '0.3',   '1.0']
er = [    '0.050', '0.050', '0.050']
epsff = [ '0.010', '0.010', '0.010']
nruns = len(mdot)

# Generate image plot
binwidth = 0.05
xlim1 = [1, 4]
ylim1 = [-3, 2.5]
xlim2 = [-0.5, 3.5]
ylim2 = [-3, 2.5]
img1 = np.zeros((int((ylim1[1]-ylim1[0])/binwidth),
                 int((xlim1[1]-xlim1[0])/binwidth),
                 3))
img1 += 1
img2 = np.zeros((int((ylim2[1]-ylim2[0])/binwidth),
                 int((xlim2[1]-xlim2[0])/binwidth),
                 3))
img2 += 1
problim = [0., 1.5]

# Loop over runs
for i in range(nruns):

    # Read latest checkpoint
    basename = osp.join(dirname, 'mdot'+mdot[i]+'_er'+er[i]+'_epsff' + \
                        epsff[i]+'_?????.vader')
    files = glob.glob(basename)
    files.sort()
    chkname = files[-1]
    data = readCheckpoint(chkname)

    # Grab data from this run
    r = data.grid.r
    vphi = data.grid.vphi_g[1:-1]
    area = data.grid.area
    t = data.tOut
    col = data.colOut
    colsfr = data.userOut[:,4,:]
    colsfr_obs = data.userOut[:,6,:]
    mdotin = -data.mBndOut[1,1]/t[1]

    # Compute derived quantities
    idx750 = r < 375*pc
    idxmax = np.argmax(np.mean(colsfr, axis=0))
    #idxmax = np.argmax(colsfr, axis=1)
    idx10 = np.abs(np.subtract.outer(r[idxmax], r)) < 10*pc
    mgas750 = np.sum(col[:,idx750]*area[idx750], axis=1)
    mgas10 = np.sum(col*idx10*area, axis=1)
    sfr750 = np.sum(colsfr[:,idx750]*area[idx750], axis=1)
    sfr750_obs = np.sum(colsfr_obs[:,idx750]*area[idx750], axis=1)
    sfr10 = np.sum(colsfr*idx10*area, axis=1)
    sfr10_obs = np.sum(colsfr_obs*idx10*area, axis=1)
    sigmag750 = mgas750/np.sum(area[idx750])
    sigmasfr750 = sfr750_obs/np.sum(area[idx750])
    sigmag10 = mgas10/np.sum(area*idx10)
    sigmasfr10 = sfr10/np.sum(area*idx10)
    torb10 = 2*np.pi*r[idxmax]/vphi[idxmax]
    torb750 = 2*np.pi*r[np.amax(np.where(r < 750*pc)[0])] / \
              vphi[np.amax(np.where(r <750*pc)[0])]

    # Make checkerboards in Sigma_gas - Sigma_SFR plane
    idx = t>200*Myr
    hist2d_750, xe, ye = np.histogram2d(
        np.log10(sigmag750[idx]/(Msun/pc**2)),
        np.log10(sigmasfr750[idx]/(Msun/pc**2/Myr)),
        bins = [(xlim1[1]-xlim1[0])/binwidth,
                (ylim1[1]-ylim1[0])/binwidth],
        range = [xlim1, ylim1],
        normed = True)
    hist2d_10, xe, ye = np.histogram2d(
        np.log10(sigmag10[idx]/(Msun/pc**2)),
        np.log10(sigmasfr10[idx]/(Msun/pc**2/Myr)),
        bins = [(xlim1[1]-xlim1[0])/binwidth,
                (ylim1[1]-ylim1[0])/binwidth],
        range = [xlim1, ylim1],
        normed = True)
    
    # Add checkerboards to image
    img1[:, :, (i+1) % 3] -= np.clip(
        (np.log10(np.transpose(hist2d_750)+1e-50)-problim[0]) /
        (problim[1]-problim[0]), 0, 1)
    img1[:, :, (i+2) % 3] -= np.clip(
        (np.log10(np.transpose(hist2d_750)+1e-50)-problim[0]) /
        (problim[1]-problim[0]), 0, 1)
    img1[:, :, (i+1) % 3] -= np.clip(
        (np.log10(np.transpose(hist2d_10)+1e-50)-problim[0]) /
        (problim[1]-problim[0]), 0, 1)
    img1[:, :, (i+2) % 3] -= np.clip(
        (np.log10(np.transpose(hist2d_10)+1e-50)-problim[0]) /
        (problim[1]-problim[0]), 0, 1)

    # Repeat procedure in Sigma_gas/t_orb - Sigma_SFR plane
    hist2d_750, xe, ye = np.histogram2d(
        np.log10(sigmag750[idx]/torb750/(Msun/pc**2/Myr)),
        np.log10(sigmasfr750[idx]/(Msun/pc**2/Myr)),
        bins = [(xlim2[1]-xlim2[0])/binwidth,
                (ylim2[1]-ylim2[0])/binwidth],
        range = [xlim2, ylim2],
        normed = True)
    hist2d_10, xe, ye = np.histogram2d(
        np.log10(sigmag10[idx]/torb10/(Msun/pc**2/Myr)),
        np.log10(sigmasfr10[idx]/(Msun/pc**2/Myr)),
        bins = [(xlim2[1]-xlim2[0])/binwidth,
                (ylim2[1]-ylim2[0])/binwidth],
        range = [xlim2, ylim2],
        normed = True)

    # Add checkerboards to image
    img2[:, :, (i+1) % 3] -= np.clip(
        (np.log10(np.transpose(hist2d_750)+1e-50)-problim[0]) /
        (problim[1]-problim[0]), 0, 1)
    img2[:, :, (i+2) % 3] -= np.clip(
        (np.log10(np.transpose(hist2d_750)+1e-50)-problim[0]) /
        (problim[1]-problim[0]), 0, 1)
    img2[:, :, (i+1) % 3] -= np.clip(
        (np.log10(np.transpose(hist2d_10)+1e-50)-problim[0]) /
        (problim[1]-problim[0]), 0, 1)
    img2[:, :, (i+2) % 3] -= np.clip(
        (np.log10(np.transpose(hist2d_10)+1e-50)-problim[0]) /
        (problim[1]-problim[0]), 0, 1)

# Make plot
plt.figure(1, figsize=(8,4.5))
plt.clf()

# First panel
ax1 = plt.subplot(1,2,1)
ax1.imshow(img1, origin='lower', interpolation='nearest',
           extent=[xlim1[0], xlim1[1], ylim1[0], ylim1[1]],
           aspect='auto')
p,=ax1.plot(xlim1, np.array(xlim1) - 3 - np.log10(2), 'k--', lw=2,
            label=r'$t_{\mathrm{dep}} = 2$, $0.2$ Gyr')
ax1.plot(xlim1, np.array(xlim1) - 2 - np.log10(2), 'k--', lw=2)
ax1.annotate('Whole CMZ', xy=(1.7, 0.0), xytext=(1.1, 1.0),
             arrowprops={ 'facecolor' : 'black', 'shrink' : 0.02,
                          'width' : 1, 'headwidth' : 7, 'headlength' : 12 })
ax1.annotate('Ring', xy=(3.0, 1.5), xytext=(2.0, 1.8),
             arrowprops={ 'facecolor' : 'black', 'shrink' : 0.02,
                          'width' : 1, 'headwidth' : 7, 'headlength' : 12 })
ax1.set_xlabel(r'$\log\,\Sigma$ [$M_\odot$ pc$^{-2}$]')
ax1.set_ylabel(r'$\log\,\dot{\Sigma}_*$ [$M_\odot$ pc$^{-2}$ Myr$^{-1}$]')
ax1.set_ylim(ylim1)
ax1.set_xticks([1, 2, 3, 4])

# Legend for first panel
rpatch = mpatches.Patch(color='red',
                        label=r'0.1 $M_\odot$ yr$^{-1}$')
gpatch = mpatches.Patch(color='green',
                        label=r'0.3 $M_\odot$ yr$^{-1}$')
bpatch = mpatches.Patch(color='blue',
                        label=r'1.0 $M_\odot$ yr$^{-1}$')
ax1.legend(handles=[rpatch, gpatch, bpatch, p], loc='lower center',
           prop={'size' : 10 }, handlelength=1, ncol=2)

ax2 = plt.subplot(1,2,2)
ax2.imshow(img2, origin='lower', interpolation='nearest',
           extent=[xlim2[0], xlim2[1], ylim2[0], ylim2[1]],
           aspect='auto')
ax2.plot(xlim2, np.array(xlim2) - 2, 'k--', lw=2)
ax2.plot(xlim2, np.array(xlim2) - 1, 'k--', lw=2)
p,=ax2.plot(xlim2, np.array(xlim2), 'k--', lw=2, label=r'$t_{\mathrm{dep}}/t_{\mathrm{orb}} = 100, 10, 1$')
ax2.annotate('Whole CMZ', xy=(0.5, 0.0), xytext=(-0.3, 1.0),
             arrowprops={ 'facecolor' : 'black', 'shrink' : 0.02,
                          'width' : 1, 'headwidth' : 7, 'headlength' : 12 })
ax2.annotate('Ring', xy=(2.5, 1.5), xytext=(1.0, 1.8),
             arrowprops={ 'facecolor' : 'black', 'shrink' : 0.02,
                          'width' : 1, 'headwidth' : 7, 'headlength' : 12 })
ax2.yaxis.set_ticklabels([])
ax2.set_ylim(ylim2)
ax2.set_xticks([0, 1, 2, 3])
ax2.set_xlabel(r'$\log\,\Sigma/t_{\mathrm{orb}}$ [$M_\odot$ pc$^{-2}$ Myr$^{-1}$]')

# Legend for 2nd panel
ax2.legend(handles=[p], loc='lower right', prop={'size' : 10})

# Adjust spacing
plt.subplots_adjust(hspace=0, wspace=0, left=0.1, right=0.85, bottom=0.15)

# Colorbars
cdictbl={ 'red' : [ (0.0, 1.0, 1.0),
                    (1.0, 0.0, 0.0) ],
          'green' : [ (0.0, 1.0, 1.0),
                      (1.0, 0.0, 0.0) ],
          'blue' : [ (0.0, 0.0, 1.0),
                     (1.0, 1.0, 1.0) ] }
cdictgr={ 'red' : [ (0.0, 0.0, 1.0),
                    (1.0, 0.0, 0.0) ],
          'green' : [ (0.0, 0.0, 1.0),
                      (1.0, 1.0, 1.0) ],
          'blue' : [ (0.0, 0.0, 1.0),
                     (1.0, 0.0, 0.0) ] }
cdictrd={ 'red' : [ (0.0, 0.0, 1.0),
                    (1.0, 1.0, 1.0) ],
          'green' : [ (0.0, 0.0, 1.0),
                      (1.0, 0.0, 0.0) ],
          'blue' : [ (0.0, 0.0, 1.0),
                     (1.0, 0.0, 0.0) ] }
cmapbl=colors.LinearSegmentedColormap('bl', cdictbl)
cmapgr=colors.LinearSegmentedColormap('gr', cdictgr)
cmaprd=colors.LinearSegmentedColormap('rd', cdictrd)
norm=colors.Normalize(vmin=problim[0], vmax=problim[1])
cbarwidth=0.015
box2 = ax2.get_position()
box2.x0 = box2.x1
box2.x1 += cbarwidth
axcbar1 = plt.gcf().add_axes(box2)
cbar.ColorbarBase(axcbar1, norm=norm, orientation='vertical', cmap=cmaprd)
plt.setp(axcbar1.get_yticklabels(), visible=False)
box2 = axcbar1.get_position()
box2.x0 = box2.x1
box2.x1 += cbarwidth
axcbar2 = plt.gcf().add_axes(box2)
cbar.ColorbarBase(axcbar2, norm=norm, orientation='vertical', cmap=cmapgr)
plt.setp(axcbar2.get_yticklabels(), visible=False)
box2 = axcbar2.get_position()
box2.x0 = box2.x1
box2.x1 += cbarwidth
axcbar3 = plt.gcf().add_axes(box2)
cb = cbar.ColorbarBase(axcbar3, norm=norm, orientation='vertical', cmap=cmapbl)
cb.set_label('Log probability density')

# Save
plt.savefig('ksplot.pdf')
