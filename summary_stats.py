# This script reads all the runs and prints out summary statistics for them

from vader import readCheckpoint
import numpy as np
import os.path as osp
import glob
from scipy.signal import periodogram

# Units
import scipy.constants as physcons
G = physcons.G*1e3
import astropy.units as astropyu
Msun = astropyu.Msun.to('g')
pc = astropyu.pc.to('cm')
yr = astropyu.yr.to('s')
Myr = 1e6*yr
Gyr = 1e9*yr
kmps = 1e5

# Directory where data lives
dirname = '/Users/krumholz/Data/cmz/test1/'

# List of run parameters
mdot = [    '0.1',   '0.3',   '0.3',   '0.3',   '0.3',   '0.3',   '1.0']
er = [    '0.050', '0.025', '0.050', '0.050', '0.050', '0.100', '0.050']
epsff = [ '0.010', '0.010', '0.005', '0.010', '0.020', '0.010', '0.010']
nruns = len(mdot)
tmin = 200*Myr

# Loop over runs
lines = []
for i in range(nruns):

    # Read latest checkpoint
    basename = osp.join(dirname, 'mdot'+mdot[i]+'_er'+er[i]+'_epsff' + \
                        epsff[i]+'_?????.vader')
    files = glob.glob(basename)
    files.sort()
    chkname = files[-1]
    data = readCheckpoint(chkname)

    # Grab data from this run
    r = data.grid.r
    vphi = data.grid.vphi_g[1:-1]
    beta = data.grid.beta_g[1:-1]
    area = data.grid.area
    t = data.tOut
    col = data.colOut
    pres = data.presOut
    sigma = np.sqrt(pres/col)
    colstar = data.userOut[:,0,:]
    colwind = data.userOut[:,1,:]
    pdot = data.userOut[:,2,:]
    colsfr = data.userOut[:,4,:]
    windrate = data.userOut[:,5,:]
    colsfr_obs = data.userOut[:,6,:]
    mdotin = -data.mBndOut[:,1]/(t+1.0e-50)
    mdotin[0] = mdotin[1]

    # Compute derived quantities
    mdotwind = np.sum(windrate*area, axis=1)
    idxmax = np.argmax(colsfr * (r>50*pc), axis=1)
    idx10 = np.abs(np.subtract.outer(r[idxmax], r)) < 10*pc
    mgas = np.sum(col*area, axis=1)
    mgas10 = np.sum(col*idx10*area, axis=1)
    sfr = np.sum(colsfr*area, axis=1)
    sfr_obs = np.sum(colsfr_obs*area, axis=1)
    sfr10 = np.sum(colsfr*idx10*area, axis=1)
    tdep = mgas / sfr
    tdep10 = mgas10 / sfr10

    # Print SF mass fraction
    print 'Stats for m{:02d}r{:03d}f{:02d}:'.format(
        int(float(mdot[i])*10), int(float(er[i])*1000),
        int(float(epsff[i])*1000))
    print '     sfr / (wind + sfr) = ' + \
        str(np.mean(sfr[t>tmin])/
            (np.mean(sfr[t>tmin])+np.mean(mdotwind[t>tmin])))

    # Compute and print timescales
    f, pxx = periodogram(sfr[t>tmin], fs=1.0/(t[1]-t[0]), window='hann')
    print '     Max timescale (true) = ' + \
        str(np.amax(1/(f[pxx/np.amax(pxx)>0.1]*Myr))) + \
        ' Myr'
    print '     Min timescale (true) = ' + \
        str(np.amin(1/(f[pxx/np.amax(pxx)>0.1]*Myr))) + ' Myr'
    f_obs, pxx_obs \
        = periodogram(sfr_obs[t>tmin], fs=1.0/(t[1]-t[0]), window='hann')
    print '     Max timescale (observed) = ' + \
        str(np.amax(1/(f_obs[pxx_obs/np.amax(pxx_obs)>0.1]*Myr))) + \
        ' Myr'
    print '     Min timescale (observed) = ' + \
        str(np.amin(1/(f_obs[pxx_obs/np.amax(pxx_obs)>0.1]*Myr))) + ' Myr'

    # Make line of latex output
    outline = 'm{:02d}r{:03d}f{:02d}   &  {:3.1f}   &   {:5.3f}   &   {:5.3f}   &   {:4.2f}   &   {:2d} ({:2d})   &   {:2d} ({:2d})  \\\\'.format(
        int(float(mdot[i])*10),
        int(float(er[i])*1000),
        int(float(epsff[i])*1000),
        float(mdot[i]),
        float(er[i]),
        float(epsff[i]),
        np.mean(sfr[t>tmin])/
            (np.mean(sfr[t>tmin])+np.mean(mdotwind[t>tmin])),
        int(np.amax(1/(f[pxx/np.amax(pxx)>0.1]*Myr))),
        int(np.amax(1/(f_obs[pxx_obs/np.amax(pxx_obs)>0.1]*Myr))),
        int(np.amin(1/(f[pxx/np.amax(pxx)>0.1]*Myr))),
        int(np.amin(1/(f_obs[pxx_obs/np.amax(pxx_obs)>0.1]*Myr)))
    )
    lines.append(outline)

for l in lines:
    print l
