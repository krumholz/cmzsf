# This script produces a cycle plot with observed quantities

from vader import readCheckpoint
import numpy as np
import numpy.ma as ma
import matplotlib.pyplot as plt
import matplotlib.colorbar as cb
import matplotlib.colors as colors
import matplotlib.patches as patches
import os.path as osp
import glob
import scipy.misc
import matplotlib.cm as cm
import matplotlib.colors as colors
import scipy.special as sps

# Units
import scipy.constants as physcons
G = physcons.G*1e3
import astropy.units as astropyu
Msun = astropyu.Msun.to('g')
pc = astropyu.pc.to('cm')
yr = astropyu.yr.to('s')
Myr = 1e6*yr
Gyr = 1e9*yr
kmps = 1e5

# Directory where data lives
dirname = '' #'/Users/krumholz/Data/cmz/test1/'

# Parameters of runs to process
mdot = '1.0'
er = '0.050'
epsff = '0.010'

# Checkpoint to process
basename = osp.join(dirname, 'mdot'+mdot+'_er'+er+'_epsff' + \
                    epsff+'_?????.vader')
outext = '_fiducial.pdf'
files = glob.glob(basename)
files.sort()
chkname = files[-1]

# Read the checkpoint
data = readCheckpoint(chkname)

# Pointers to quantities we want
r = data.grid.r
r_h = data.grid.r_h
vphi = data.grid.vphi_g[1:-1]
beta = data.grid.beta_g[1:-1]
omega = vphi/r
kappa = np.sqrt(2*(beta+1))*omega
area = data.grid.area
t = data.tOut
col = data.colOut
pres = data.presOut
sigma = np.sqrt(pres/col)
colstar = data.userOut[:,0,:]
colwind = data.userOut[:,1,:]
pdot = data.userOut[:,2,:]
edot = data.userOut[:,3,:]
colsfr = data.userOut[:,4,:]
colwind = data.userOut[:,5,:]
colsfr_obs = data.userOut[:,6,:]
mdotin = -data.mBndOut[:,1]/(t+1.0e-50)
mdotin[0] = mdotin[1]

# Compute scale height, virial ratio, depletion time, Q
rhostar = 2.5*vphi**2*(1+2*beta)/(4*np.pi*G*r**2)
zetad = 0.33
a = 2.0*np.pi*zetad*G*rhostar*col
b = np.pi/2.0*G*col**2
c = -pres
h = (-b + np.sqrt(b**2-4*a*c))/(2*a)
alphavir = pres/(np.pi/2.0*G*col**2*h)
tdep = col/(colsfr+1.0e-50)
rho = col/(2*h)
Q = kappa*sigma/(np.pi*G*col)

# Calculate PDF-related quantities
mu = 2.3
mh = 1.7e-24
ecore = .5
phit = 1.91
phix = 1.12
cs = 5e4
mach = sigma/cs/np.sqrt(3.)
sigPDF = np.sqrt(np.log(1.+.48*mach**2.))
meanPDF = -sigPDF**2./2.
meanrhoPDF = np.exp(meanPDF)*rho
rhocrit = phix**2.*np.pi**2./15.*alphavir*mach**2.*rho
xcrit=rhocrit/rho
sfrff = .5*ecore/phit*(1.+sps.erf((-2.*np.log(rhocrit/rho)+sigPDF**2.)/(2.**1.5*sigPDF)))
frac4 = .5*(1.+sps.erf((-2.*np.log(1.e4*mu*mh/rho)+sigPDF**2.)/(2.**1.5*sigPDF)))
frac5 = .5*(1.+sps.erf((-2.*np.log(1.e5*mu*mh/rho)+sigPDF**2.)/(2.**1.5*sigPDF)))
frac6 = .5*(1.+sps.erf((-2.*np.log(1.e6*mu*mh/rho)+sigPDF**2.)/(2.**1.5*sigPDF)))
fraccrit = .5*(1.+sps.erf((-2.*np.log(xcrit)+sigPDF**2.)/(2.**1.5*sigPDF)))

# Time index to plot
tidx = [4820,4851,4869,4895]
ntidx = np.size(tidx)

# Make grid for Galactic coordinates map
Rgc = 8.5e3*pc    # Assumed GC distance
lmax = 3.
nl = 256
lgrid = np.linspace(-lmax,lmax,nl)
nidx = (ntidx,nl)
rho_grid = np.zeros(nidx)+1e-50
rhocrit_grid = np.zeros(nidx)+1e-50
xcrit_grid = np.zeros(nidx)+1e-50
frac4_grid = np.zeros(nidx)+1e-50
frac5_grid = np.zeros(nidx)+1e-50
frac6_grid = np.zeros(nidx)+1e-50
fraccrit_grid = np.zeros(nidx)+1e-50
l_r = np.arcsin(r/Rgc)*360./2./np.pi

# Position of Sgr A*, from SIMBAD; all coordinates are relative to it
lsgra = 359.9442 - 360
bsgra = -0.0462

# Loop through bins in l
for i, l in enumerate(lgrid):

    for j, ti in enumerate(tidx):
    
        # Displacement of this los from galactic center
        d = Rgc*np.sin(l*2*np.pi/360.)
    
        # Indices of annuli this ray intersects
        idx = np.where(r_h[1:] > np.abs(d))[0]
        
        # Get path length of ray through annulus; handle special case of a
        # ray that only interects the annulus once
        s = 2*(np.sqrt(r_h[idx+1]**2-d**2) -
               np.sqrt(np.maximum(r_h[idx]**2-d**2, 0.0)))
        
        # Get mass-weighted gas fractions and critical densities
        wgt = col[ti,idx]*s
        rho_grid[j,i] = np.sum(rho[ti,idx]*wgt)/np.sum(wgt)/mu/mh
        rhocrit_grid[j,i] = np.sum(rhocrit[ti,idx]*wgt)/np.sum(wgt)/mu/mh
        xcrit_grid[j,i] = np.sum(xcrit[ti,idx]*wgt)/np.sum(wgt)
        frac4_grid[j,i] = np.sum(frac4[ti,idx]*wgt)/np.sum(wgt)
        frac5_grid[j,i] = np.sum(frac5[ti,idx]*wgt)/np.sum(wgt)
        frac6_grid[j,i] = np.sum(frac6[ti,idx]*wgt)/np.sum(wgt)
        fraccrit_grid[j,i] = np.sum(fraccrit[ti,idx]*wgt)/np.sum(wgt)


# Make two plots
tint = 20
skip = 2
nstart = col[:,0].size - 1 - 10*tint
plt.figure(1, figsize=(6,16))
plt.clf()
ax1 = plt.gcf().add_subplot(7,1,1)
ax2 = plt.gcf().add_subplot(7,1,2)
ax3 = plt.gcf().add_subplot(7,1,3)
ax4 = plt.gcf().add_subplot(7,1,4)
ax5 = plt.gcf().add_subplot(7,1,5)
ax6 = plt.gcf().add_subplot(7,1,6)
ax7 = plt.gcf().add_subplot(7,1,7)
plt.subplots_adjust(hspace=0, wspace=0, bottom=0.1, top=0.95, left=0.12, right=0.85)
colormap=cm.jet

fmin=1.e-4
fmax=0.95

# Plot rho
for j, ti in enumerate(tidx):
    linecolor = (t[ti]-t[nstart])/Myr/tint
    ax1.plot(lgrid, rho_grid[j,:],c=colormap(linecolor))
#    ax1.plot(l_r, rho[ti,:]/mu/mh,':',c=colormap(linecolor))
#    ax1.plot(-l_r, rho[ti,:]/mu/mh,':',c=colormap(linecolor))
ax1.set_yscale('log')
ax1.set_xlim([-lmax,lmax])
ax1.set_ylim([1e0,1e6])
ax1.set_ylabel(r'$n$ [cm$^{-3}$]')
plt.setp(ax1.get_xticklabels(), visible=False)

# Plot rhocrit
for j, ti in enumerate(tidx):
    linecolor = (t[ti]-t[nstart])/Myr/tint
    ax2.plot(lgrid, rhocrit_grid[j,:],c=colormap(linecolor))
#    ax2.plot(l_r, rhocrit[ti,:]/mu/mh,':',c=colormap(linecolor))
#    ax2.plot(-l_r, rhocrit[ti,:]/mu/mh,':',c=colormap(linecolor))
ax2.set_yscale('log')
ax2.set_xlim([-lmax,lmax])
ax2.set_ylim([1e5,5e6])
ax2.set_ylabel(r'$n_{\rm crit}$ [cm$^{-3}$]')
plt.setp(ax2.get_xticklabels(), visible=False)

# Plot xcrit
for j, ti in enumerate(tidx):
    linecolor = (t[ti]-t[nstart])/Myr/tint
    ax3.plot(lgrid, rhocrit_grid[j,:]/rho_grid[j,:],c=colormap(linecolor))
#    ax3.plot(l_r, rhocrit[ti,:]/mu/mh,':',c=colormap(linecolor))
#    ax3.plot(-l_r, rhocrit[ti,:]/mu/mh,':',c=colormap(linecolor))
ax3.set_yscale('log')
ax3.set_xlim([-lmax,lmax])
ax3.set_ylim([1e0,.9e6])
ax3.set_ylabel(r'$x_{\rm crit}=n_{\rm crit}/n$')
plt.setp(ax3.get_xticklabels(), visible=False)

# Plot f_dense4
labels = ['post-burst (2 Myr)','minimum (5 Myr)','pre-burst (7 Myr)','burst (9 Myr)']
for j, ti in enumerate(tidx):
    linecolor = (t[ti]-t[nstart])/Myr/tint
    ax4.plot(lgrid, frac4_grid[j,:],c=colormap(linecolor), label=labels[j])
#    ax4.plot(l_r, frac4[ti,:],':',c=colormap(linecolor))
#    ax4.plot(-l_r, frac4[ti,:],':',c=colormap(linecolor))
ax4.set_yscale('log')
ax4.set_xlim([-lmax,lmax])
ax4.set_ylim([fmin,fmax])
ax4.set_ylabel(r'$f(n>10^4 {\rm cm}^{-3})$')
plt.setp(ax4.get_xticklabels(), visible=False)
ax4.legend(loc='lower center', numpoints=1, prop={'size':10}, ncol=2)

# Plot f_dense5
for j, ti in enumerate(tidx):
    linecolor = (t[ti]-t[nstart])/Myr/tint
    ax5.plot(lgrid, frac5_grid[j,:],c=colormap(linecolor))
#    ax5.plot(l_r, frac5[ti,:],':',c=colormap(linecolor))
#    ax5.plot(-l_r, frac5[ti,:],':',c=colormap(linecolor))
ax5.set_yscale('log')
ax5.set_xlim([-lmax,lmax])
ax5.set_ylim([fmin,fmax])
ax5.set_ylabel(r'$f(n>10^5 {\rm cm}^{-3})$')
plt.setp(ax5.get_xticklabels(), visible=False)

# Plot f_dense6
for j, ti in enumerate(tidx):
    linecolor = (t[ti]-t[nstart])/Myr/tint
    ax6.plot(lgrid, frac6_grid[j,:],c=colormap(linecolor))
#    ax6.plot(l_r, frac6[ti,:],':',c=colormap(linecolor))
#    ax6.plot(-l_r, frac6[ti,:],':',c=colormap(linecolor))
ax6.set_yscale('log')
ax6.set_xlim([-lmax,lmax])
ax6.set_ylim([fmin,fmax])
ax6.set_ylabel(r'$f(n>10^6 {\rm cm}^{-3})$')
plt.setp(ax6.get_xticklabels(), visible=False)

# Plot f_crit
for j, ti in enumerate(tidx):
    linecolor = (t[ti]-t[nstart])/Myr/tint
    ax7.plot(lgrid, fraccrit_grid[j,:],c=colormap(linecolor))
#    ax7.plot(l_r, fraccrit[ti,:],':',c=colormap(linecolor))
#    ax7.plot(-l_r, fraccrit[ti,:],':',c=colormap(linecolor))
ax7.set_yscale('log')
ax7.set_xlim([-lmax,lmax])
ax7.set_ylim([fmin,fmax])
ax7.set_xlabel(r'$\ell$ [deg]')
ax7.set_ylabel(r'$f(n>n_{\rm crit})$')

# Add colorbar
cbarwidth=0.02
cbbox = ax7.get_position()
cbbox.x0 = cbbox.x1
cbbox.x1 = cbbox.x1+cbarwidth
cbbox.y1 = cbbox.y0 + 7*(cbbox.y1-cbbox.y0)
axcbar = plt.gcf().add_axes(cbbox)
norm = colors.Normalize(vmin=0, vmax=tint)
cbar = cb.ColorbarBase(axcbar, norm=norm, orientation='vertical')
cbar.set_label(r'$t$ [Myr]')

# Save
figsave = plt.gcf()
figsave.set_size_inches(6,14)
plt.savefig('crit_dens_all.pdf')





plt.figure(1, figsize=(6,6))
plt.clf()
ax1 = plt.gcf().add_subplot(2,1,1)
ax2 = plt.gcf().add_subplot(2,1,2)
plt.subplots_adjust(hspace=0, wspace=0, bottom=0.1, top=0.97, left=0.12, right=0.95)

j = 1
# Plot rho, rhocrit
ax1.plot(lgrid, rhocrit_grid[j,:],c='r',label='Critical density',lw=2)
ax1.plot(lgrid, rho_grid[j,:],c='black',label='Midplane density',lw=2)
ax1.set_yscale('log')
ax1.set_xlim([-lmax,lmax])
ax1.set_ylim([1e0,9e6])
ax1.set_ylabel(r'$n$ [cm$^{-3}$]')
plt.setp(ax1.get_xticklabels(), visible=False)
ax1.legend(loc='center right', numpoints=1, prop={'size':10}, ncol=1)

# Plot f_dense{4,5,6,ncrit}
ax2.plot(lgrid, frac4_grid[j,:],'-',c='black',label='$n_{\mathrm{ref}}=10^4 \mathrm{cm}^{-3}$',lw=2)
ax2.plot(lgrid, frac5_grid[j,:],'--',c='black',label='$n_{\mathrm{ref}}=10^5 \mathrm{cm}^{-3}$',lw=2)
ax2.plot(lgrid, frac6_grid[j,:],':',c='black',label='$n_{\mathrm{ref}}=10^6 \mathrm{cm}^{-3}$',lw=2)
ax2.plot(lgrid, fraccrit_grid[j,:],c='r',label='$n_{\mathrm{ref}}=n_{\mathrm{crit}}$',lw=2)
ax2.set_yscale('log')
ax2.set_xlim([-lmax,lmax])
ax2.set_ylim([.1*fmin,fmax])
ax2.set_xlabel(r'$\ell$ [deg]')
ax2.set_ylabel(r'$f(n>n_{\rm ref})$')
ax2.legend(loc='lower center', numpoints=1, prop={'size':10}, ncol=2)

# Save
figsave = plt.gcf()
figsave.set_size_inches(6,6)
plt.savefig('crit_dens.pdf')










