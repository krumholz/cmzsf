# This script runs a single vader simulation

# Import libraries
import argparse
import vader
import numpy as np
from ctypes import c_double, c_long, POINTER, byref
from astropy.io.ascii import read as asciiread
import scipy.constants as physcons
import astropy.units as astropyu
import os.path as osp

# Read arguments
parser = argparse.\
         ArgumentParser(description = 'Run a vader simulation of the CMZ')
parser.add_argument('paramfile',
                    help='template parameter file name')
parser.add_argument('-o', '--outdir', default='.',
                    help='output directory name')
parser.add_argument('-r', '--restart',
                    default = None,
                    help='name of file from which to restart')
parser.add_argument('-m', '--mdot', type=float,
                    default = None,
                    help='accretion rate in Msun/yr (overrides value in paramfile)')
parser.add_argument('-er', '--er', type=float, default=None,
                    help='radial variation of SFR (overrides value in paramfile)')
parser.add_argument('-ef', '--epsff0', type=float, default=None,
                    help='SFR per free-fall time at alpha_vir = 1 (overrides value in paramfile)')
parser.add_argument('-ew', '--enwind', default=None,
                    action='store_true',
                    help='use energy-driven wind model (overrides value in paramfile)')
parser.add_argument('-t', '--trun', type=float, default=500,
                    help='run time in Myr')
args = parser.parse_args()

# Units
G = physcons.G*1e3
Msun = astropyu.Msun.to('g')
pc = astropyu.pc.to('cm')
yr = astropyu.yr.to('s')
Myr = 1e6*yr
kmps = 1e5

# Read the template parameter file
paramDict = vader.readParam(args.paramfile)

# Override arguments
if args.mdot is not None:
    mdot = args.mdot
    paramDict['obc_pres_val'] = -mdot*Msun/yr
else:
    mdot = -float(paramDict['obc_pres_val'])/(Msun/yr)
if args.er is not None:
    er = args.er
    paramDict['star_rad_var'] = er
else:
    er = float(paramDict['star_rad_var'])
if args.epsff0 is not None:
    epsff0 = args.epsff0
    paramDict['epsff0'] = epsff0
else:
    epsff0 = float(paramDict['epsff0'])
if args.enwind is not None:
    if args.enwind:
        enwind = 1.0
    else:
        enwind = -1.0
else:
    if int(paramDict['en_wind']) > 0:
        enwind = 1.0
    else:
        enwind = -1.0

# Get mass versus r and build vphi vs. r
mvsr = asciiread(paramDict['m_vs_r_file'])
ndata = len(mvsr['col1'].data)
rotcurvetab = np.zeros((2, ndata))
rotcurvetab[0,:]=mvsr['col1'].data*pc
rotcurvetab[1,:]=np.sqrt(G*mvsr['col2'].data*Msun/rotcurvetab[0,:])

# Construct grid
grd = vader.grid(paramDict, rotCurveTab=rotcurvetab)

# Construct output names
basename = 'mdot{:03.1f}_er{:05.3f}_epsff{:05.3f}'. \
           format(mdot, er, epsff0, enwind > 0)
checkname = osp.join(args.outdir, basename)

# Initialize column density and pressure
col = np.zeros(grd.nr)+paramDict['init_col']*Msun/pc**2
pres = col*(paramDict['init_vdisp']*kmps)**2
gamma = paramDict['gamma']
paramDict['ibc_enth_val'] = gamma/(gamma-1.0) * pres[0]/col[0]

# Set up parameters to pass to c; note that entry 6 is actually a
# long, but the byte representation of 0 long and 0 double is the
# same
paramArr = np.zeros(14+1005*grd.r.size, dtype='d')
paramArr[0] = paramDict['eta']
paramArr[1] = paramDict['sigmath']*kmps
paramArr[2] = paramDict['shapefac']
paramArr[3] = paramDict['zetad']
paramArr[4] = paramDict['alphacoef']
paramArr[5] = paramDict['epsff0']
paramArr[6] = 0.0
paramArr[7] = paramDict['p_per_sn']*Msun*kmps
paramArr[8] = paramDict['e_per_sn']
paramArr[9] = paramDict['star_rad_var']
paramArr[10] = paramDict['col_min']*Msun/pc**2
paramArr[11] = paramDict['sigma_max']*kmps
paramArr[12] = paramDict['col_floor']*Msun/pc**2
paramArr[13] = enwind
paramPtr = byref(paramArr.ctypes.
                 data_as(POINTER(c_double)).
                 contents)
userCum = np.array([True,True,False,False,False,False,False,False])

# Set up output time and run times
tRun = args.trun
tSaveStep = 0.1
tSave = np.linspace(0, tRun*Myr, int(tRun/tSaveStep)+1)
checkInt = 100
chk = np.zeros(tSave.size, dtype=bool)
chk[::checkInt] = True

# Launch simulation
if args.restart is None:
    vaderOut = vader.vader(0, tSave[-1], paramDict,
                           grd=grd, col=col, pres=pres,
                           saveTimes = tSave,
                           c_params = paramPtr, nUserOut = 8,
                           userCum = userCum,
                           checkpoint = chk, checkname = checkname)
else:
    vaderOut = vader.vader(0, tSave[-1], paramDict,
                           restart = args.restart,
                           saveTimes = tSave,
                           c_params = paramPtr, nUserOut = 8,
                           userCum = userCum,
                           checkpoint = chk, checkname = checkname)
