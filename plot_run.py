# This script produces plots summarizing the output of a run

from vader import readCheckpoint
import numpy as np
import numpy.ma as ma
import matplotlib.pyplot as plt
import matplotlib.colorbar as cb
import os.path as osp
import glob
from scipy.signal import periodogram

# Units
import scipy.constants as physcons
G = physcons.G*1e3
import astropy.units as astropyu
Msun = astropyu.Msun.to('g')
pc = astropyu.pc.to('cm')
yr = astropyu.yr.to('s')
Myr = 1e6*yr
Gyr = 1e9*yr
kmps = 1e5

# Directory where data lives
dirname = '/Users/krumholz/Data/cmz/test1'
#dirname = '/Users/krumholz/Projects/cmz3/scripts'

# Parameters of runs to process
mdot = '1.0'
er = '0.050'
epsff = '0.010'
enwind = '0'

# Checkpoint to process
#basename = osp.join(dirname, 'mdot'+mdot+'_er'+er+'_epsff' + \
#                    epsff+'_enwind'+enwind+'_?????.vader')
basename = osp.join(dirname, 'mdot'+mdot+'_er'+er+'_epsff' + \
                    epsff+'_?????.vader')
outext = '_fiducial.pdf'
files = glob.glob(basename)
files.sort()
chkname = files[-1]

# Read the checkpoint
data = readCheckpoint(chkname)

# Pointers to quantities we want
r = data.grid.r
vphi = data.grid.vphi_g[1:-1]
beta = data.grid.beta_g[1:-1]
omega = vphi/r
kappa = np.sqrt(2*(beta+1))*omega
area = data.grid.area
t = data.tOut
col = data.colOut
pres = data.presOut
sigma = np.sqrt(pres/col)
colstar = data.userOut[:,0,:]
colwind = data.userOut[:,1,:]
pdot = data.userOut[:,2,:]
edot = data.userOut[:,3,:]
colsfr = data.userOut[:,4,:]
colwind = data.userOut[:,5,:]
colsfr_obs = data.userOut[:,6,:]
mdot = data.userOut[:,7,:]
mdotin = -data.mBndOut[:,1]/(t+1.0e-50)
mdotin[0] = mdotin[1]

# Compute scale height, virial ratio, depletion time
rhostar = 2.5*vphi**2*(1+2*beta)/(4*np.pi*G*r**2)
zetad = 0.33
a = 2.0*np.pi*zetad*G*rhostar*col
b = np.pi/2.0*G*col**2
c = -pres
h = (-b + np.sqrt(b**2-4*a*c))/(2*a)
alphavir = pres/(np.pi/2.0*G*col**2*h)
tdep = col/(colsfr+1.0e-50)

# Make space-time plots
xlim = [1.0, 2.5]
ylim = [0, 500]
xticklocs = [1.0, 1.5, 2.0]
xticklocs1 = xticklocs + [2.5]
#xlim = np.log10(np.array([r[0]/pc, r[-1]/pc]))
plt.figure(1, figsize=(8,8))
plt.clf()

ax = plt.gcf().add_subplot(1,1,1)
ax1 = plt.gcf().add_subplot(3,3,1)
ax2 = plt.gcf().add_subplot(3,3,2)
ax3 = plt.gcf().add_subplot(3,3,3)
ax4 = plt.gcf().add_subplot(3,3,4)
ax5 = plt.gcf().add_subplot(3,3,5)
ax6 = plt.gcf().add_subplot(3,3,6)
ax7 = plt.gcf().add_subplot(3,3,7)
ax8 = plt.gcf().add_subplot(3,3,8)
ax9 = plt.gcf().add_subplot(3,3,9)
plt.subplots_adjust(hspace=0.2, wspace=0.04, bottom=0.1, top=0.92, 
                    left=0.1, right=0.9)

im = ax1.imshow(np.log10(col/(Msun/pc**2)), origin='lower',
                aspect='auto', vmin=1, vmax=4,
                extent=(np.log10(r[0]/pc), np.log10(r[-1]/pc),
                        t[0]/Myr, t[-1]/Myr))
ax1.set_xlim(xlim)
ax1.set_ylim(ylim)
ax1.get_xaxis().set_ticks([])
cax, kw = cb.make_axes(ax1, location='top', pad=0)
plt.colorbar(im, cax, label=r'$\log\, \Sigma$ [M$_\odot$ pc$^{-2}$]',
             orientation='horizontal', ticks=[1,2,3,4])
cax.xaxis.set_ticks_position('top')
cax.xaxis.set_label_position('top')

im = ax2.imshow(np.log10(sigma/kmps), origin='lower',
                aspect='auto', vmin=-0.5, vmax=2.5,
                extent=(np.log10(r[0]/pc), np.log10(r[-1]/pc),
                        t[0]/Myr, t[-1]/Myr))
ax2.set_xlim(xlim)
ax2.set_ylim(ylim)
ax2.get_xaxis().set_ticks([])
ax2.get_yaxis().set_ticks([])
cax, kw = cb.make_axes(ax2, location='top', pad=0)
plt.colorbar(im, cax, label=r'$\log\, \sigma$ [km s$^{-1}$]',
             orientation='horizontal', ticks=[0, 1, 2])
cax.xaxis.set_ticks_position('top')
cax.xaxis.set_label_position('top')

im = ax3.imshow(np.log10(h/pc), origin='lower',
                aspect='auto', vmin=-0.5, vmax=2.5,
                extent=(np.log10(r[0]/pc), np.log10(r[-1]/pc),
                        t[0]/Myr, t[-1]/Myr))
ax3.set_xlim(xlim)
ax3.set_ylim(ylim)
ax3.get_xaxis().set_ticks([])
ax3.get_yaxis().set_ticks([])
cax, kw = cb.make_axes(ax3, location='top', pad=0)
plt.colorbar(im, cax, label=r'$\log\, H$ [pc]',
             orientation='horizontal', ticks=[0, 1, 2])
cax.xaxis.set_ticks_position('top')
cax.xaxis.set_label_position('top')

im = ax4.imshow(alphavir, origin='lower',
                aspect='auto', vmin=1, vmax=4,
                extent=(np.log10(r[0]/pc), np.log10(r[-1]/pc),
                        t[0]/Myr, t[-1]/Myr))
ax4.set_xlim(xlim)
ax4.set_ylim(ylim)
ax4.get_xaxis().set_ticks([])
cax, kw = cb.make_axes(ax4, location='top', pad=0)
plt.colorbar(im, cax=cax, label=r'$\alpha_{\mathrm{vir}}$',
             orientation='horizontal', ticks=[1,2,3])
cax.xaxis.set_ticks_position('top')
cax.xaxis.set_label_position('top')

im = ax5.imshow(np.log10(tdep/Gyr), origin='lower',
                aspect='auto', vmin=-1.5, vmax=0.5,
                extent=(np.log10(r[0]/pc), np.log10(r[-1]/pc),
                        t[0]/Myr, t[-1]/Myr))
ax5.set_xlim(xlim)
ax5.set_ylim(ylim)
ax5.get_xaxis().set_ticks([])
ax5.get_yaxis().set_ticks([])
cax, kw = cb.make_axes(ax5, location='top', pad=0)
plt.colorbar(im, cax=cax, label=r'$\log\,t_{\mathrm{dep}}$ [Gyr]',
             orientation='horizontal', ticks=[-1.5, -1, -0.5, 0])
cax.xaxis.set_ticks_position('top')
cax.xaxis.set_label_position('top')

im = ax7.imshow(np.log10((colsfr+1e-50)/(Msun/pc**2/Myr)), origin='lower',
                aspect='auto', vmin=-1.5, vmax=1.5,
                extent=(np.log10(r[0]/pc), np.log10(r[-1]/pc),
                        t[0]/Myr, t[-1]/Myr))
ax7.set_xlim(xlim)
ax7.set_ylim(ylim)
ax7.get_xaxis().set_ticks(xticklocs)
cax, kw = cb.make_axes(ax7, location='top', pad=0)
plt.colorbar(im, cax, 
            label=r'$\log\, \dot{\Sigma}_*$ [M$_\odot$ pc$^{-2}$ Myr$^{-1}$]',
            orientation='horizontal', ticks=[-1,0,1])
cax.xaxis.set_ticks_position('top')
cax.xaxis.set_label_position('top')

im = ax8.imshow(np.log10((colwind+1e-50)/(Msun/pc**2/Myr)), origin='lower',
                aspect='auto', vmin=-1.5, vmax=1.5,
                extent=(np.log10(r[0]/pc), np.log10(r[-1]/pc),
                        t[0]/Myr, t[-1]/Myr))
ax8.set_xlim(xlim)
ax8.set_ylim(ylim)
ax8.get_yaxis().set_ticks([])
ax8.get_xaxis().set_ticks(xticklocs)
cax, kw = cb.make_axes(ax8, location='top', pad=0)
plt.colorbar(im, cax,
             label=r'$\log\, \dot{\Sigma}_{\mathrm{wind}}$ [M$_\odot$ pc$^{-2}$ Myr$^{-1}$]',
             orientation='horizontal', ticks=[-1,0,1])
cax.xaxis.set_ticks_position('top')
cax.xaxis.set_label_position('top')

if enwind == '0':
    im = ax9.imshow(np.log10((pdot+1e-50)), origin='lower',
                    aspect='auto', vmin=-10, vmax=-7,
                    extent=(np.log10(r[0]/pc), np.log10(r[-1]/pc),
                            t[0]/Myr, t[-1]/Myr))
else:
    im = ax9.imshow(np.log10((edot+1e-50)), origin='lower',
                    aspect='auto', vmax=-3.5, vmin=-5.5,
                    extent=(np.log10(r[0]/pc), np.log10(r[-1]/pc),
                            t[0]/Myr, t[-1]/Myr))
ax9.set_xlim(xlim)
ax9.set_ylim(ylim)
ax9.get_xaxis().set_ticks(xticklocs1)
ax9.get_yaxis().set_ticks([])
cax, kw = cb.make_axes(ax9, location='top', pad=0)
if enwind == '0':
    plt.colorbar(im, cax, label=r'$\log\, d\dot{p}/dA$ [dyne cm$^{-2}$]',
                 orientation='horizontal',
                 ticks=[-10,-9,-8,-7])
cax.xaxis.set_ticks_position('top')
cax.xaxis.set_label_position('top')

ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.tick_params(top='off', bottom='off', left='off', right='off')
ax.patch.set_visible(False)
ax.get_xaxis().set_ticks([])
ax.get_yaxis().set_ticks([])
ax.set_ylabel(r'$t$ [Myr]', labelpad=30)
ax.set_xlabel(r'$\log\,r$ [pc]', labelpad=20)

# Add rotation curve
ax6.spines['top'].set_color('none')
ax6.spines['bottom'].set_color('none')
ax6.spines['left'].set_color('none')
ax6.spines['right'].set_color('none')
ax6.tick_params(top='off', bottom='off', left='off', right='off')
ax6.patch.set_visible(False)
ax6.get_xaxis().set_ticks([])
ax6.get_yaxis().set_ticks([])

ax6abox = ax6.get_position()
dy = ax6abox.height
ax6abox.y0 = ax6abox.y0 + 0.4525*dy
ax6abox.y1 = ax6abox.y0 + 0.4525*dy
ax6a = plt.gcf().add_axes(ax6abox)
ax6a.plot(np.log10(r/pc), vphi/kmps, 'k', lw=2)
ax6a.get_xaxis().set_ticklabels([])
ax6a.get_yaxis().tick_right()
ax6a.get_yaxis().set_label_position('right')
ax6a.set_ylim([0,200])
ax6a.set_xlim(xlim)
ax6a.set_ylabel(r'$v_{\phi}$ [km s$^{-1}$]')
ax6a.set_title('Rotation curve, shear')

ax6bbox = ax6.get_position()
ax6bbox.y1 = ax6abox.y0
ax6b = plt.gcf().add_axes(ax6bbox)
ax6b.plot(np.log10(r/pc), 1-beta, 'k', lw=2)
ax6b.get_xaxis().set_ticklabels([])
ax6b.get_yaxis().tick_right()
ax6b.get_yaxis().set_label_position('right')
ax6b.set_ylim([0,1.4])
ax6b.get_yaxis().set_ticks([0,0.5,1.0])
ax6b.set_xlim(xlim)
ax6b.set_ylabel(r'$1-\beta$')

#plt.savefig('summary'+outext)

# Plot total rates
mdotstar = np.sum(colsfr*area, axis=1)
mdotwind = np.sum(colwind*area, axis=1)
mdotstar_obs = np.sum(colsfr_obs*area, axis=1)
plt.figure(2, figsize=(6,4))
plt.clf()
plt.plot(t/Myr, mdotstar/(Msun/yr), 'b', lw=2, label='SF')
plt.plot(t/Myr, mdotstar_obs/(Msun/yr), 'b--', lw=2, label='SF (observed)')
plt.plot(t/Myr, mdotwind/(Msun/yr), 'r', lw=2, label='Wind')
plt.plot(t/Myr, mdotin/(Msun/yr), 'k--', lw=2, label='Inflow')
plt.xlim([0,500])
plt.yscale('log')
plt.ylim([1e-2*mdotin[0]/(Msun/yr), 20*mdotin[0]/(Msun/yr)])
plt.legend(loc='upper center', ncol=2)
plt.xlabel(r'$t$ [Myr]')
plt.ylabel(r'$\dot{M}$ [$M_\odot$ yr$^{-1}$]')
plt.subplots_adjust(left=0.18, bottom=0.15)
#plt.savefig('sfr'+outext)

# Plot depletion time, gas mass, virial ratio on various scales
idx500 = np.where(r < 250*pc)[0]
#idxmax = np.argmax(colsfr*(r > 50*pc), axis=1)
idxmax = np.argmax(np.mean(colsfr, axis=0))
idx10 = np.abs(np.subtract.outer(r[idxmax], r)) < 10*pc
mgas = np.sum(col*area, axis=1)
mgas500 = np.sum(col[:,idx500]*area[idx500], axis=1)
mgas10 = np.sum(col*idx10*area, axis=1)
sfr = mdotstar
sfr500 = np.sum(colsfr[:,idx500]*area[idx500], axis=1)
sfr10 = np.sum(colsfr*idx10*area, axis=1)
sfr_obs = mdotstar_obs
sfr500_obs = np.sum(colsfr_obs[:,idx500]*area[idx500], axis=1)
sfr10_obs = np.sum(colsfr_obs*idx10*area, axis=1)
tdep = mgas/(mdotstar+1e-50)
tdep500 = mgas500/(sfr500+1e-50)
tdep10 = mgas10/(sfr10+1e-50)
tdep_obs = mgas/(mdotstar_obs+1e-50)
tdep500_obs = mgas500/(sfr500_obs+1e-50)
tdep10_obs = mgas10/(sfr10_obs+1e-50)

# Plot depletion time and gas mass
plt.figure(3, figsize=(6,6))
plt.clf()

ax1 = plt.gcf().add_subplot(2,1,1)
ax2 = plt.gcf().add_subplot(2,1,2)
plt.subplots_adjust(hspace=0, wspace=0, top=0.95)

ax1.plot(t/Myr, tdep/Gyr, 'b', lw=2, label=r'$t_{\mathrm{dep}}$')
ax1.plot(t/Myr, tdep10/Gyr, 'r', lw=2, label=r'$t_{\mathrm{dep},10}$')
ax1.plot(t/Myr, tdep_obs/Gyr, 'b--', lw=2, label=r'$t_{\mathrm{dep}}$')
ax1.plot(t/Myr, tdep10_obs/Gyr, 'r--', lw=2, label=r'$t_{\mathrm{dep},10}$')
ax1.set_yscale('log')
ax1.set_xlim([0,500])
ax1.set_ylim([0.01,10])
ax1.get_xaxis().set_ticks([])
ax1.set_ylabel(r'$t_{\mathrm{dep}}$ [Gyr]')
#ax1.legend(loc='lower left', title='True')

ax2.plot(t/Myr, mgas/Msun, 'b', lw=2, label='All')
ax2.plot(t/Myr, mgas10/Msun, 'r', lw=2, label='10 pc ring')
ax2.set_yscale('log')
ax2.set_xlim([0,500])
ax2.set_ylim([5e6,2e8])
#ax2.set_ylim([0,37])
ax2.legend(loc='upper center', ncol=2)
ax2.set_ylabel(r'$M_{\mathrm{gas}}$ [$10^6$ $M_\odot$]')
ax2.set_xlabel(r'$t$ [Myr]')

#plt.savefig('tdep_mgas'+outext)


# Make periodogram and probability density plot, excluding first 100 Myr
if len(sfr) > 1000:
    f, pxx = periodogram(sfr[1000:], fs=1.0/(t[1]-t[0]), window='hann')
    f, pxx_obs = periodogram(sfr_obs[1000:], fs=1.0/(t[1]-t[0]), window='hann')
    plt.figure(4, figsize=(6,4))
    plt.clf()
    plt.plot(1/(f*Myr), pxx/np.amax(pxx), 'k', lw=2, label='SFR')
    plt.plot(1/(f*Myr), pxx_obs/np.amax(pxx_obs), 'k--', lw=2, label='SFR (observed)')
    plt.xlim([1,100])
    plt.ylim([0,1.1])
    plt.xscale('log')
    plt.legend(loc='upper left')
    plt.xlabel(r'$\nu^{-1}$ [Myr]')
    plt.ylabel('Relative power')
    plt.subplots_adjust(bottom=0.15)
    #plt.savefig('periodogram'+outext)

    binwidth = 0.1
    binedges = np.arange(-2,1.0001,binwidth)
    hist, edges = np.histogram(np.log10(tdep[t>100*Myr]/Gyr),
                               binedges,
                               density=True)
    tdepctr = 0.5*(edges[1:]+edges[:-1])
    hist10, edges = np.histogram(np.log10(tdep10[t>100*Myr]/Gyr),
                                 binedges,
                                 density=True)
    hist_obs, edges = np.histogram(np.log10(tdep_obs[t>100*Myr]/Gyr),
                                   binedges,
                                   density=True)
    hist10_obs, edges = np.histogram(np.log10(tdep10_obs[t>100*Myr]/Gyr),
                                     binedges,
                                     density=True)
    plt.figure(5, figsize=(6,4))
    plt.clf()
    plt.plot(tdepctr, hist, 'k', lw=2, label='All')
    plt.plot(tdepctr, hist_obs, 'k--', lw=2, label='SF (observed)')
    #plt.plot(tdepctr, hist10_obs, 'r', lw=2, label='10 pc ring (observed)')
    plt.plot(np.log10(np.array([2,2])), [10.**-1.5,10], 'k:', lw=1)
    plt.xlim([-2,1])
    plt.ylim([10.**-1.5,10])
    plt.yscale('log')
    plt.legend(loc='upper left')
    plt.xlabel(r'$\log\,t_{\mathrm{dep}}$ [Gyr]')
    plt.ylabel('Probability density')
    plt.subplots_adjust(bottom=0.15)
    #plt.savefig('tdep_pdf'+outext)

# Write snapshots to text files
interval = 10
fp = open('rotcurve.txt', 'w')
fp.write('#        r           vphi          kappa           beta\n')
for i in range(r.shape[0]):
    fp.write("{:10.6f}     {:10.6f}     {:10.6f}     {:10.6f}\n".
             format(r[i]/pc, vphi[i]/kmps, kappa[i]*Myr,
                    beta[i]))
fp.close()

fp = open('surfden.txt', 'w')
fp.write('#        r  ')
for tOut in t[::interval]:
    fp.write("     t={:4.1f}  ".format(tOut/Myr))
fp.write('\n')
for i in range(col.shape[1]):
    fp.write("{:10.6f}".format(r[i]/pc))
    for j in range(0,col.shape[0],interval):
        fp.write("  {:10.6e}".format(col[j,i]/(Msun/pc**2)))
    fp.write("\n")
fp.close()

fp = open('pres.txt', 'w')
fp.write('#        r  ')
for tOut in t[::interval]:
    fp.write("     t={:4.1f}  ".format(tOut/Myr))
fp.write('\n')
for i in range(col.shape[1]):
    fp.write("{:10.6f}".format(r[i]/pc))
    for j in range(0,col.shape[0],interval):
        fp.write("  {:10.6e}".format(pres[j,i]))
    fp.write("\n")
fp.close()

fp = open('vdisp.txt', 'w')
fp.write('#        r  ')
for tOut in t[::interval]:
    fp.write("   t={:4.1f}  ".format(tOut/Myr))
fp.write('\n')
for i in range(sigma.shape[1]):
    fp.write("{:10.6f}".format(r[i]/pc))
    for j in range(0,sigma.shape[0],interval):
        fp.write("  {:10.6f}".format(sigma[j,i]/kmps))
    fp.write("\n")
fp.close()

fp = open('alpha.txt', 'w')
fp.write('#        r  ')
for tOut in t[::interval]:
    fp.write("   t={:4.1f}  ".format(tOut/Myr))
fp.write('\n')
for i in range(alphavir.shape[1]):
    fp.write("{:10.6f}".format(r[i]/pc))
    for j in range(0,alphavir.shape[0],interval):
        fp.write("  {:10.6f}".format(alphavir[j,i]))
    fp.write("\n")
fp.close()

fp = open('scaleheight.txt', 'w')
fp.write('#        r  ')
for tOut in t[::interval]:
    fp.write("   t={:4.1f}  ".format(tOut/Myr))
fp.write('\n')
for i in range(h.shape[1]):
    fp.write("{:10.6f}".format(r[i]/pc))
    for j in range(0,h.shape[0],interval):
        fp.write("  {:10.6f}".format(h[j,i]/pc))
    fp.write("\n")
fp.close()
